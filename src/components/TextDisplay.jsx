import React, { Fragment, useState } from 'react'
import Output from './Output';

const TextDisplay = () => {
  const [changedText, setChangedText] = useState(false);
  const changedTextHandler = ()=>{
    setChangedText(true);
  }
  return (
    <Fragment>

     <h2>Hello World</h2> 
    {!changedText &&  <Output>How Are You ?</Output>}
    {changedText && <Output>Changed!</Output>}
    <button onClick={changedTextHandler}>Change Text</button>
    </Fragment>
  )
}

export default TextDisplay