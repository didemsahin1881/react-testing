import {screen, render} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import TextDisplay from "./TextDisplay"

describe("Text Display component",()=>{

    test("renders hello world as a test", ()=>{
        //Arrange
        render(<TextDisplay/>)
    
        // Act
        //--- 
        
        //Assert
        //---
    const helloWorldElement = screen.getByText("Hello World");
    expect(helloWorldElement).toBeInTheDocument();
    });

    test("renders How Are You as a test", ()=>{
        //Arrange
        render(<TextDisplay/>);    
        // Act
        
        //Assert
    const howAreYouElement = screen.getByText("How Are You ?");
    expect(howAreYouElement).toBeInTheDocument();
    });

    test("renders 'Changed!'if the button was clicked", async ()=>{
        //Arrange
        render(<TextDisplay/>);    
        // Act
        const buttonElement = screen.getByRole("button");
        await userEvent.click(buttonElement);
        //Assert
        const outputElement = screen.getByText("Changed!");
        expect(outputElement).toBeInTheDocument();
    
    });
});
